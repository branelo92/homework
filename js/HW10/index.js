let tabs = document.querySelectorAll('.tabs .tabs-title');
for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", () => tabChange(i));
}
function tabChange(number = 0){
    let contentActive = document.querySelector('.tabs-content .active');
    if(contentActive){
        contentActive.classList.remove('active');
    }
    document.querySelectorAll('.tabs-content li')[number].classList.add('active');

    let tabActive = document.querySelector('.tabs .active');
    if(tabActive){
        tabActive.classList.remove('active');
    }
    document.querySelectorAll('.tabs .tabs-title')[number].classList.add('active');
}
tabChange();

