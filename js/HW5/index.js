function getPrompt(message){
    let answer;
    do{
        answer = prompt(message);
    } while(answer === '' || answer === null);
    return answer;
}

function createNewUser(){
    const newUser = {};
    newUser.firstName = getPrompt('Введіть ім\'я');
    newUser.lastName = getPrompt('Введіть прізвище');
    newUser.getLogin = function() {
        let login = this.firstName.substring(0, 1) + this.lastName.replace(/\s/g, '');
        return login.toLowerCase();
    }
    return newUser;
}

console.log(createNewUser().getLogin());



