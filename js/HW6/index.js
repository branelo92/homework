function getPrompt(message){
    let answer;
    do{
        answer = prompt(message);
    } while(answer === '' || answer === null);
    return answer;
}

function createNewUser(){
    const newUser = {};
    newUser.firstName = getPrompt('Введіть ім\'я');
    newUser.lastName = getPrompt('Введіть прізвище');
    newUser.birthday = getPrompt('Введіть дату народження у форматі dd.mm.yyyy');
    newUser.getLogin = function() {
        let login = this.firstName.substring(0, 1) + this.lastName.replace(/\s/g, '');
        return login.toLowerCase();
    }
    newUser.getAge = function(){
        let today = new Date();
        let birthday = this.birthday;
        let ageUser = today.getFullYear() - birthday.substring(6, 10);
        let month = today.getMonth() - birthday.substring(3, 5) + 1;
        if( month < 0 || month === 0 && today.getDate() < birthday.substring(0, 2)){
            ageUser--;
        }
        return ageUser;
    }
    newUser.getPassword = function() {
        let password = this.firstName.substring(0, 1) + this.lastName.replace(/\s/g, '') + this.birthday.substring(6, 10);
        return password.toLowerCase();
    }
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());


