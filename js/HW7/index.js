function filterBy(array, type){

    let result = [];
    array.forEach((element) => {
        if (type !== typeof element) result.push(element);
    });
    return result;

}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
