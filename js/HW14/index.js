let body = document.querySelector("body");


document.getElementById('theme').onclick = function(){
    if(localStorage.getItem('black-theme')){
        localStorage.removeItem('black-theme');
        body.classList.remove('black-theme');
    } else {
        localStorage.setItem('black-theme', true);
        body.classList.add('black-theme');
    }
}

if(localStorage.getItem('black-theme')){
    body.classList.add('black-theme');
}







// HW13
let images = document.querySelectorAll('.images-wrapper .image');
let i = 0;
let seconds = 1;
let status = true;

setTimeout(slider, seconds * 1000);


const fragment = new DocumentFragment();
const btn = document.createElement('a');
btn.innerText = 'Припинити';
btn.id = "stop";
btn.className = "btn";
fragment.append(btn);
const btn2 = document.createElement('a');
btn2.innerText = 'Відновити показ';
btn2.id = "play";
btn2.className = "btn";
fragment.append(btn2);
document.body.append(fragment);

document.getElementById('stop').onclick = function(){
    status = false;
}
document.getElementById('play').onclick = function(){
    if (!status){
        status = true;
        setTimeout(slider, seconds * 1000);
    }
}


function slider() {
    document.querySelector('.images-wrapper .image-to-show').classList.remove('image-to-show');
    i++;
    if (i >= images.length) i = 0;
    images[i].classList.add('image-to-show');
    if (status) setTimeout(slider, seconds * 1000);


}
