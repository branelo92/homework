let tabs = document.querySelectorAll('.tabs_name .tabs-title');
for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", () => tabChange(i + 1));
}

function tabChange(number) {
    let contentActive = document.querySelector('.tabs-content .active');
    if (contentActive) {
        contentActive.classList.remove('active');
    }
    document.querySelector('.tabs-content li[data-tab="' + number + '"]').classList.add('active');

    let tabActive = document.querySelector('.tabs .active');
    if (tabActive) {
        tabActive.classList.remove('active');
    }
    document.querySelector('.tabs .tabs-title[data-tab="' + number + '"]').classList.add('active');
}


tabs = document.querySelectorAll('.works_title .href');
for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener("click", () => tabsWork(tabs[i].getAttribute('data-category')));
}

function tabsWork(category) {
    let titleActive = document.querySelector('.works_title .active');
    if (titleActive) {
        titleActive.classList.remove('active');
    }
    document.querySelector('.works_title .href[data-category="' + category + '"').classList.add('active');

    let worksPortfolio = document.querySelectorAll('.works_portfolio .li');
    for (let i = 0; i < worksPortfolio.length; i++) {
        if (worksPortfolio[i].getAttribute('data-category') === category || category === 'All') {
            worksPortfolio[i].classList.remove('hide');
        } else {
            worksPortfolio[i].classList.add('hide');
        }
    }
}

document.getElementById('btn-add').addEventListener("click", function () {
    const portfolio = {
        1: {
            category: 'Graphic Design',
            name: 'creative design',
            img: 'img/graphic-design3.jpg',
            url1: '#',
            url2: '#'
        },
        2: {
            category: 'Web Design',
            name: 'creative design',
            img: 'img/web-design3.jpg',
            url1: '#',
            url2: '#'
        },
        3: {
            category: 'Landing Pages',
            name: 'creative design',
            img: 'img/landing-page4.jpg',
            url1: '#',
            url2: '#'
        },
        4: {
            category: 'Graphic Design',
            name: 'creative design',
            img: 'img/graphic-design4.jpg',
            url1: '#',
            url2: '#'
        },
        5: {
            category: 'Landing Pages',
            name: 'creative design',
            img: 'img/landing-page5.jpg',
            url1: '#',
            url2: '#'
        },
        6: {
            category: 'Web Design',
            name: 'creative design',
            img: 'img/web-design4.jpg',
            url1: '#',
            url2: '#'
        },
        7: {
            category: 'Wordpress',
            name: 'creative design',
            img: 'img/wordpress2.jpg',
            url1: '#',
            url2: '#'
        },
        8: {
            category: 'Wordpress',
            name: 'creative design',
            img: 'img/wordpress3.jpg',
            url1: '#',
            url2: '#'
        },
        9: {
            category: 'Web Design',
            name: 'creative design',
            img: 'img/web-design5.jpg',
            url1: '#',
            url2: '#'
        },
        10: {
            category: 'Graphic Design',
            name: 'creative design',
            img: 'img/graphic-design5.jpg',
            url1: '#',
            url2: '#'
        },
        11: {
            category: 'Wordpress',
            name: 'creative design',
            img: 'img/wordpress4.jpg',
            url1: '#',
            url2: '#'
        },
        12: {
            category: 'Graphic Design',
            name: 'creative design',
            img: 'img/graphic-design6.jpg',
            url1: '#',
            url2: '#'
        }
    }

    const fragment = new DocumentFragment();
    for (let block in portfolio) {
        const li = document.createElement('li');
        li.classList.add('li');
        let statusCategory = document.querySelector('.works_title .active').getAttribute('data-category');
        if(statusCategory !== portfolio[block]['category'] && statusCategory !== 'All'){
            li.classList.add('hide');
        }
        li.setAttribute("data-category", portfolio[block]['category']);
        li.innerHTML = `
        <img src="${portfolio[block]['img']}" alt="" class="img">
        <div class="block">
            <div class="url_wrap">
                <a href="${portfolio[block]['url1']}" class="url"></a>
                <a href="${portfolio[block]['url2']}" class="url_square"></a>
            </div>
            <div class="name">
                ${portfolio[block]['name']}
            </div>
            <div class="category">
                 ${portfolio[block]['category']}
            </div>
        </div>
        `;

        fragment.append(li)
    }
    document.getElementById('works_portfolio').append(fragment);

    document.getElementById('btn-add').style.display='none';

});

