let btns = document.querySelectorAll('.btn-wrapper .btn');
let keys = [];
for (let i = 0; i < btns.length; i++) {
    if(btns[i].outerText === 'Enter'){
        keys.push(btns[i].outerText);
    } else{
        keys.push('Key' + btns[i].outerText);
    }
}
document.addEventListener('keydown', function(event) {
    let num = keys.indexOf(event.code);
    if(num !== -1){
        let active = document.querySelector('.btn-wrapper .btn.active');
        if(active){
            active.classList.remove('active');

        }
        document.querySelectorAll('.btn-wrapper .btn')[num].classList.add('active');
    }
});







