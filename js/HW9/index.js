function createList(arr, position = null){
    const fragment = new DocumentFragment();
    const ul = document.createElement('ul');
    let i = 0;
    for (let i = 0; i < arr.length; i++) {
        let li = document.createElement('li');
        li.innerText = arr[i];
        ul.append(li);
    }
    fragment.append(ul);
    if (position){
        document.querySelector(position).append(fragment);
    } else {
        document.body.append(fragment);
    }
}


createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
createList(["1", "2", "3", "sea", "user", 23]);

