// 1
let paragrafs = document.querySelectorAll('p');
for (let i = 0; i < paragrafs.length; i++) {
    paragrafs[i].style.background = '#fff';
}

// 2
let optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentNode);
let childOptionsList = optionsList.childNodes;
if (childOptionsList){
    [].forEach.call(childOptionsList, function(elem) {
        console.log(elem.tagName);
        console.log(elem.nodeType)
    });
}

// 3
document.getElementById('testParagraph').innerText = 'This is a paragraph';

// 4 Получить элементы <li>, вложенные в элемент с классом main-header и вывеcти их в консоль. Каждому из элементов присвоить новый класс nav-item.
let mainHeader = document.querySelectorAll('.main-header li');
for (let i = 0; i < mainHeader.length; i++) {
    console.log(mainHeader[i]);
    mainHeader[i].classList.add('nav-item');
}

// 5 Найти все элементы с классом section-title. Удалить этот класс у элементов.
let sectionTitle = document.querySelectorAll('.section-title');
for (let i = 0; i < sectionTitle.length; i++) {
    sectionTitle[i].classList.remove('section-title');
}






