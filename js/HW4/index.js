function getNumber(message){
    let userNumber;
    do{
        if(userNumber === null) { userNumber = '';}
        userNumber = prompt(message, userNumber);
    } while(isNaN(+userNumber) || userNumber === '' || userNumber === null);
    return +userNumber;
}

function getOperation(message){
    let userOperation;
    do{
        if(userOperation === null) { userOperation = '';}
        userOperation = prompt(message, userOperation);
    } while(!(userOperation === '+' || userOperation === '-' || userOperation === '*' || userOperation === '/'));
    return userOperation;
}

function showResult(a, b, operation){
    let result;
    switch (operation) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
    }
    return result;

}

const numeric1 = getNumber('Введіть перше число');
const numeric2 = getNumber('Введіть Друге число');
const operation = getOperation('Введіть математичну операцію. "+", "-", "*" або "/"');

console.log(showResult(numeric1, numeric2, operation));




